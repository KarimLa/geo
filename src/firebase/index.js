import firebase from "firebase";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyBxrDIk_4Icu2w2THDjFo3061zu9W3LWsg",
  authDomain: "online-business-kai.firebaseapp.com",
  databaseURL: "https://online-business-kai.firebaseio.com",
  projectId: "online-business-kai",
  storageBucket: "online-business-kai.appspot.com",
  messagingSenderId: "1061872559390",
  appId: "1:1061872559390:web:5e8b0dbb36b202ad"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore();
