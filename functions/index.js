const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

exports.checkAlias = functions.https.onCall(async (data, context) => {
  const ref = admin
    .firestore()
    .collection("users")
    .doc(data.slug);

  try {
    const doc = await ref.get();
    return {
      unique: !doc.exists
    };
  } catch (_) {
    throw new functions.https.HttpsError("failed to connect");
  }
});
